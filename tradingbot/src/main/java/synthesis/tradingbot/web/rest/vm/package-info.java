/**
 * View Models used by Spring MVC REST controllers.
 */
package synthesis.tradingbot.web.rest.vm;
