package synthesis.tradingbot.web.rest.util;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.sagemakerruntime.AmazonSageMakerRuntime;
import com.amazonaws.services.sagemakerruntime.AmazonSageMakerRuntimeClientBuilder;
import com.amazonaws.services.sagemakerruntime.model.InvokeEndpointRequest;
import com.amazonaws.services.sagemakerruntime.model.InvokeEndpointResult;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.boot.json.JsonParseException;
import org.springframework.web.multipart.MultipartFile;
import synthesis.tradingbot.web.rest.PredictionsResource;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * CSV Manipulation methods can be found here
 */
public class PredictionsResourceHelper {

    private final Logger log = LoggerFactory.getLogger(PredictionsResourceHelper.class);

    /**
     * This method receives a comma-separated CSV file as input. It stores it into a BufferedReader and readies to read
     * each line. Format : 'date,open,high,low,close,volume,average'
     * We initialize an empty string to being empty. This string will contain
     * For each line in the input file, it splits the line by a comma and stores it into a temporary array
     * Then, it concatenates the string
     * @param csv_inference_file
     * @return
     */
    public String read_csv(File csv_inference_file){
        // String csvFile = "C:\\Users\\Sontaga\\Desktop\\dummy_make_inferences_jan.csv";
        String line = "";
        String temp = "";
        StringBuilder builder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(csv_inference_file))) {
            while ((line = br.readLine()) != null) {
                //temp.concat(line);
                char[] stringToChar = line.toCharArray();
                for (int i = 0; i < stringToChar.length; i++) {
                    builder.append(stringToChar[i]);
                }
                builder.append('\n');
            }
            builder.delete(builder.length()-1,builder.length());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.substring(2,builder.length());
    }
    /**
     * This method processes JSON-encoded string with this format:
     *  "predictions": [
     *         {
     *             "score": 0.4
     *         },
     *         {
     *             "score": 0.2
     *         }
     *  ]
     * TODO:Implement handling of the result being in an alternative format which will cause the method to return null
     * @param final_prediction
     * @return array of predictions where each position corresponds to the prediction of that entry
     */
    public Double[] processPredictionsJSON(String modelName ,String final_prediction) {
        ArrayList<Double> prediction_set = new ArrayList<>();
        JSONParser parser = new JSONParser();
        JSONArray jsonArray = new JSONArray();
        JSONObject js_obj = new JSONObject();
        if(modelName.contains("Linear")){
            try {

                js_obj = (JSONObject)parser.parse(final_prediction);
                jsonArray = (JSONArray) js_obj.get("predictions");
            } catch (ParseException e) {
                e.printStackTrace();
            }
            // NOTE : There are no predictions to be made if the parameter cannot be converted to JSON
            if(jsonArray != null){
                for (int i = 0; i < jsonArray.size(); i++) {
                    JSONObject object = (JSONObject) jsonArray.get(i);
                    String score = object.get("score").toString();
                    prediction_set.add(Double.parseDouble(score.replaceAll("\"","")));
                }
            }
        }else if(modelName.contains("Gradient")){
            String[] predictionSet = final_prediction.split(",");

            for (int i = 0; i < predictionSet.length; i++) {
                prediction_set.add(Double.parseDouble(predictionSet[i]));
            }
        }
        return prediction_set.toArray(new Double[prediction_set.size()]);
    }

    /**
     * This method extracts all timestamps in the supplied csv_inference_file  and stores them in
     * the returned Double[] array
     * @param length
     * @param csv_inference_file
     * @return
     */
    public Double[] getTimestamps(String modelName,int length, File csv_inference_file) {
        String line = "";
        StringBuilder builder = new StringBuilder();
        Double[] matrix = new Double[length];
        ArrayList<Double> temp_timestamps = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csv_inference_file))) {
            if(modelName.contains("Linear")){
                while ((line = br.readLine()) != null) {
                    //temp.concat(line);
                    String[]splitted = line.split(",");
                    String time = splitted[0].replaceAll("\"","");
                    Double temp = Double.parseDouble(time);
                    temp_timestamps.add(temp);
                }
                //builder.delete(builder.length()-2,builder.length()-1);
            }else if(modelName.contains("Gradient")){
                while ((line = br.readLine()) != null) {
                    //temp.concat(line);
                    String[]splitted = line.split(" ");
                    String time_tet = splitted[1];
                    String[] temp_time = time_tet.split(":");
                    String time = temp_time[1];
                    Double temp = Double.parseDouble(time);
                    temp_timestamps.add(temp);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return temp_timestamps.toArray(new Double[length]);
    }

    /**
     * This method creates a JSON array object which is to be written and sent to AWS S3.
     * @param predictions : This is the array of predictions made by the model
     * @param timestamp : This is the array of timestamps from the supplied data/
 *                        This is required because a graph of predicted price vs time will be drawn
     * @return
     */
    public String createJSON(Double[] predictions, Double[] timestamp) {
        JSONArray sample = new JSONArray();
        int pred_len = predictions.length;int timestamps_len = timestamp.length;
        if((predictions.length >0 && timestamp.length> 0) && (pred_len == timestamps_len)){
            StringBuilder builder = new StringBuilder();
            String open_brack = "[";
            String close_brac = "]";
            String open_json_brack = "{";
            String close_json_brack = "}";
            builder.append(open_brack);
            for (int i = 0; i < predictions.length; i++) {
                builder.append('\n')
                    .append(open_brack)
                    .append('\n')
                    .append(timestamp[i])
                    .append(',')
                    .append('\n')
                    .append(predictions[i])
                    .append('\n')
                    .append(close_brac);
                if((i+1) != predictions.length){
                    builder.append(',');
                }
            }
            builder.append(close_brac);
            JSONParser jsonParser = new JSONParser();
            try{
                String result = builder.toString();
                sample = (JSONArray) jsonParser.parse(result);

            } catch (ParseException e) {
                e.printStackTrace();
            }
            return builder.toString();
        }else {
            return "";
        }
    }

    /**
     * This method is invoked whenever a user requires model inferences
     * @param body : input data to receive inferences from
     * @param endpoint_name : the name of the endpoint to invoke
     * @param contentType : the specified input type which the model supports.
     *                      Linear is text/csv and XGBoost is text/libsvm
     * @return : this is the result prediction(s) received from invoking the model endpoint
     */
    public String invokeModelEndpoint(String body, String endpoint_name, String contentType) {
        String final_body = body.replaceAll("\\\"","");
        // The docs for the ingestion format are here: https://docs.aws.amazon.com/sagemaker/latest/dg/cdf-inference.html
        InvokeEndpointRequest invokeEndpointRequest = new InvokeEndpointRequest();
        // Content type set here MUST match content type in the defined model
        invokeEndpointRequest.setContentType(contentType);
        // Inferences from model endpoint are in JSON format
        invokeEndpointRequest.setAccept(contentType);
        invokeEndpointRequest.setSdkRequestTimeout(20000000);
        invokeEndpointRequest.setEndpointName(endpoint_name);
        invokeEndpointRequest.setBody(ByteBuffer.wrap(final_body.getBytes()));

        AmazonSageMakerRuntime amazonSageMaker = AmazonSageMakerRuntimeClientBuilder.defaultClient();
        InvokeEndpointResult invokeEndpointResult = amazonSageMaker.invokeEndpoint(invokeEndpointRequest);

        return new String(invokeEndpointResult.getBody().array(), StandardCharsets.UTF_8 );
    }

    /**
     * This method is invoked whenever a user requests to send prediction data(JSON file) to the S3 bucket
     * so that front-end is ab
     * @param predictionsFile
     * @param bucket_name
     */
    public boolean uploadFileToS3(AmazonS3 client, InputStream predictionsFile, String bucket_name, String fileName){
            String[] temp = fileName.split("\\.");
            String prefix = "predictions/" + temp[0] + ".json";
            try {
            // Upload a file as a new object with ContentType and title specified.
            ObjectMetadata metadata = new ObjectMetadata();
            PutObjectRequest request = new PutObjectRequest(bucket_name, prefix, predictionsFile,metadata)
                .withCannedAcl(CannedAccessControlList.PublicRead);
            metadata.setContentType("application/json");
            request.setMetadata(metadata);
            client.putObject(request);
        }
        catch (AmazonS3Exception e){
            return false;
        }
        catch(AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
            return false;
        }
        catch(SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            return false;
        }
        return true;
    }

    public File convertMultipartFile(MultipartFile file){
        File f = new File(file.getOriginalFilename());
        try (FileOutputStream fos = new FileOutputStream(f,false)) {
            byte[] imageByte = file.getBytes();
            fos.write(imageByte);
            return f;
        } catch (Exception e) {
            return null;
        }
    }
}
