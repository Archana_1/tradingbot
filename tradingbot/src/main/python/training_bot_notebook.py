## from sagemaker import get_execution_role
import boto3
import botocore
from pathlib import Path
import csv
import numpy as np
from pandas import DataFrame, read_csv
import pandas as pd #this is how I usually import pandas
import matplotlib.pyplot as plt
from sagemaker.amazon.amazon_estimator import get_image_uri
from sagemaker import get_execution_role
import sagemaker.amazon.common as smac
import sagemaker
import io
import os
from sagemaker.predictor import csv_serializer, json_deserializer


'''
    Variables related to AWS
'''
s3 = boto3.resource('s3')
prefix = 'sagemaker/linear-tradingbot'
sess = sagemaker.Session()
role = get_execution_role()
bucket = 'trading-bot-bucket' # Use the name of your s3 bucket here

'''*************************************************************'''

'''
    Variables related to training, testing and validating data
    Methods related to checking if files have been downloaded from the S3 bucket
'''
# training data
training_data_fileName = "FINAL_training_data_jan_july.csv"
# testing data
testing_data_fileName = 'testing_data_august_oct.csv'
# validation data
validation_data_fileName = 'validation_data_november.csv'
# use Path() package for checking if files retrieved
path_training = Path(training_data_fileName)
path_testing = Path(testing_data_fileName)
path_validation = Path(validation_data_fileName)
# check if files have been retrived from S3 Bucket
if(path_training.is_file() and path_testing.is_file() and path_validation.is_file()):
    print("files exists")
else:
    try:
        s3.Bucket(bucket).download_file(training_data_fileName, training_data_fileName)
        s3.Bucket(bucket).download_file(testing_data_fileName, testing_data_fileName)
        s3.Bucket(bucket).download_file(validation_data_fileName, validation_data_fileName)
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("Files have not yet been uploaded to s3 bucket.")
        else:
            raise

# We are here iff files have been downloaded to the notebook
'''*****************************************************************************************************'''

'''
Prepare training data :
                        -Convert csv data(training,testing,validating) to dataframe using the pandas library
                        -Retrieve labels/targets from csv's as vectors from related csv files
'''
#headers = initializeData(training_data_fileName)
training_data_df = pd.read_csv(training_data_fileName)
testing_data_df = pd.read_csv(testing_data_fileName)
validation_data_df = pd.read_csv(validation_data_fileName)
# Convert to numpy array
numpy_training_matrix = training_data_df.loc[:, ['unix_timestamp','open','high','close','volume']].as_matrix()
numpy_testing_data_matrix = testing_data_df.loc[:, ['unix_timestamp','open','high','close','volume']].as_matrix()
numpy_validation_data_matrix = testing_data_df.loc[:, ['unix_timestamp','open','high','close','volume']].as_matrix()
# convert dataframes to numpy arrays for training, testing and validation array
training_data_labels = np.asarray(training_data_df.loc[:, ['average']])
testing_data_labels = np.asarray(testing_data_df.loc[:,['average']])
validation_data_labels = np.asarray(validation_data_df.loc[:,['average']])

'''
    Final preparation of matrices formattin to float32
'''
final_training_matrix = np.asarray(numpy_training_matrix,dtype="float32")
final_testing_matrix = np.asarray(numpy_testing_data_matrix,dtype="float32")
final_validation_matrix = np.asarray(numpy_validation_data_matrix,dtype="float32")

'''
    Convert list of list in labels numpy array to an array of values
'''
training_matrix_targets = []
testing_matrix_targets = []
validation_matrix_targets = []
for x in training_data_labels:
    training_matrix_targets.append(x[0])
for x in testing_data_labels:
    testing_matrix_targets.append(x[0])
for x in validation_data_labels:
    validation_matrix_targets.append(x[0])

final_training_data_targets = np.asarray(training_matrix_targets,dtype="float32")
final_testing_data_targets = np.asarray(training_matrix_targets,dtype="float32")
final_validation_data_targets = np.asarray(training_matrix_targets,dtype="float32")

'''******************************************************************************************************'''

'''
    Upload training data to S3 in the format required
'''
buf = io.BytesIO()
smac.write_numpy_to_dense_tensor(buf,final_training_matrix,final_training_data_targets)
buf.seek(0)
key = 'recordio-pb-data'
output_location = 's3://{}/{}/output'.format(bucket, prefix)
boto3.resource('s3').Bucket(bucket).Object(os.path.join(prefix, 'train', key)).upload_fileobj(buf)
s3_train_data = 's3://{}/{}/train/{}'.format(bucket, prefix, key)
print('uploaded training data location: {}'.format(s3_train_data))
'''*****************************************************************************************************'''

'''
    Traing the model
'''

container = get_image_uri(boto3.Session().region_name, 'linear-learner')

linear = sagemaker.estimator.Estimator(container,
                                       role,
                                       train_instance_count=1,
                                       train_instance_type='ml.m5.large',
                                       train_volume_size=50,
                                       train_max_run=360000,
                                       input_mode='File',
                                       output_path=output_location,
                                       sagemaker_session=sess)
linear.set_hyperparameters(feature_dim=5,
                           predictor_type='regressor',
                           mini_batch_size=200)
linear.fit({'train': s3_train_data},logs=False)
print("Done")
linear_predictor = linear.deploy(initial_instance_count=1,
                                 instance_type='ml.m5.large')
linear_predictor.content_type = 'text/csv'
linear_predictor.serializer = csv_serializer
linear_predictor.deserializer = json_deserializer

predictions = []
for array in np.array_split(final_testing_matrix, 100):
    result = linear_predictor.predict(array)
    print(result)
    predictions += [r['average'] for r in result['predictions']]

pd.crosstab(np.where(final_testing_data_targets == 0, 1, 0), predictions, rownames=['actuals'], colnames=['predictions'])
