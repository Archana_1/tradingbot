import { NgModule } from '@angular/core';

import { TradingbotSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [TradingbotSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [TradingbotSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class TradingbotSharedCommonModule {}
