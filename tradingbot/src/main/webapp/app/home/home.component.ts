import { Component } from '@angular/core';
import { FusionCharts } from 'fusioncharts/core';
import { Column2D } from 'fusioncharts/viz/column2d';
import { FusionTheme } from 'fusioncharts/themes/es/fusioncharts.theme.fusion';
import { Http } from '@angular/http';
import { AwsRequestsService } from './aws-requests.service';
import * as Highcharts from 'highcharts/highstock';
import { Observable } from 'rxjs/internal/Observable';
import S3 = require('aws-sdk/clients/s3');

var AWS = require('aws-sdk');
//var sagemakerRuntime = require('../../../../../node_modules/aws-sdk/clients/sagemakerruntime');
var creds = new AWS.Credentials({
    accessKeyId: 'AKIAIP25T5KYHXWSSDVA',
    secretAccessKey: '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'
});

var params = {
    DurationSeconds: 3600,
    ExternalId: '123ABC',
    // Policy: "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Sid\":\"Stmt1\",\"Effect\":\"Allow\",\"Action\":\"s3:*\",\"Resource\":\"*\"}]}",
    RoleArn: 'arn:aws:iam::882591789525:role/admin-role',
    RoleSessionName: 'admin'
};
var sts = new AWS.STS();
sts.assumeRole(params, function(err, data) {
    if (err) console.log(err, err.stack);
    // an error occurred
    else console.log(data); // successful response
});
//var smRuntime = new AWS.SageMakerRuntime({ region: 'eu-west-1', credentials: creds });
AWS.config.region = 'us-west-1';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: ['home.scss']
})
export class HomeComponent {
    // AWS Access
    model_name = '';
    file_location = '';
    selectedFiles: FileList;
    for_training = true;
    error = '';
    description = 'Upload data-set To S3 Bucket For Training Or Predictions';
    bucket_location = '';
    bucket_name = 'trading-bot-training-data-bucket';
    bucket = new S3({
        accessKeyId: creds.accessKeyId,
        secretAccessKey: creds.secretAccessKey,
        region: 'us-east-1'
    });
    // bucketList= this.bucket.listBuckets();
    data_input = {};
    file_url = 'https://s3.us-east-2.amazonaws.com/trading-bot-training-data-bucket/training_data_average_over_time.json';
    bucket_inference_URL = 'https://s3-eu-west-1.amazonaws.com/trading-bot-bucket/predictions/';
    bucketDirectories = [];
    modelNames = [];
    uploadReasons = [];
    reason = '';
    ngOnInit() {}
    constructor(private http: Http, private aws_service: AwsRequestsService) {
        // Load options for a user uploading data(send to S3 Bucket)
        // They either want to train a model or get inferences from the model
        this.uploadReasons.push('Train Model');
        this.uploadReasons.push('Make Predictions');

        // Load model option choices
        // There are 2 models currently supported : Gradient boosting and Linear Regression
        this.modelNames.push('Gradient Boosting');
        this.modelNames.push('Linear Regression');

        this.bucket
            .listObjects({ Bucket: this.bucket_name })
            .on('success', response => {
                // do something with response.data
                for (let i = 0; i < response.data['Contents'].length; i++) {
                    let temp = response.data['Contents'][i].Key.split('/');
                    console.log(temp.length);
                    if (temp.length >= 2) {
                        if (!(this.bucketDirectories.indexOf(temp[0]) > -1)) {
                            this.bucketDirectories.push(temp[0]);
                        }
                    }
                }
                return response.data['Contents'];
            })
            .send();
        //this.getResultsProxy();

        this.getJSON().subscribe(data => {
            // Create the chart
            // Create the chart
            this.data_input = JSON.parse(data.text().toString());
            //console.log(data.text());
            // Create the chart
            Highcharts.stockChart('container', {
                rangeSelector: {
                    selected: 1
                },

                title: {
                    text: 'Average BTC Price(USD) Over Time'
                },

                series: [
                    {
                        name: 'BTC Price(USD)',
                        data: this.data_input,
                        type: 'spline',
                        tooltip: {
                            valueDecimals: 2
                        }
                    }
                ]
            });
        });
    }

    public getJSON(): Observable<any> {
        return this.http.get(this.file_url);
    }

    /**
     *This method processes the user request to post to a specified s3 bucket
     * @param file that the user wants to submit to the bucket
     */
    uploadFile() {
        if (this.bucket_location.trim().length > 2) {
            // This is if it's a straightforward submit to a directory for later inferences/training
            const bucket = new S3({
                accessKeyId: 'AKIAIP25T5KYHXWSSDVA',
                secretAccessKey: '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',
                region: 'us-east-1'
            });

            // Check if the user supplied a location for the s3 bucket directory
            if (this.bucket_location.trim().length <= 1) {
                // set to a default bucket
                this.bucket_location = 'trading-bot-training-data-bucket';
            }
            const params = {
                Bucket: this.bucket_name.trim(),
                Key: this.bucket_location + '/' + this.getFile().name,
                Body: this.getFile()
            };

            if (
                bucket.upload(params, function(err, data) {
                    if (err) {
                        return false;
                    }
                    return true;
                })
            ) {
                this.error = 'Successfully uploaded file.';
            } else {
                this.error = 'There was an error uploading your file';
            }
        }
        console.log(this.error);
    }

    /**
     * This method assesses whether the user wants model inferences or just upload to S3
     * bucket
     */
    submit() {
        if (this.reason == 'Make Predictions') {
            // Create endpoint
            this.createEndpoint();
        } else {
            // Upload to S3 Bucket for training
            this.uploadFile();
        }
    }
    /**
     * Sets the file which the user wants to upload
     * @param event is the file from the front-end
     */
    setFile(event) {
        this.selectedFiles = event.target.files;
        this.file_location = event.target.result;
    }

    /**
     * Gets the file which the user wants to upload
     * @returns {File}
     */
    getFile(): File {
        return this.selectedFiles.item(0);
    }

    /**
     * Sets the name of the directory inside the bucket defined {bucket_name}
     * @param value
     */
    setBucketDirectoryName(value) {
        this.bucket_location = value;
    }

    /**
     * Sets the reason for user Uploading data from the front-end
     * @param value : reason from font-end dropdown
     */
    setUploadReason(value) {
        this.reason = value.trim().toString();
        if (this.reason == 'Train Model') {
            this.for_training = true;
        } else {
            this.for_training = false;
        }
    }

    /**
     * Sets the name of the model the user would like inferences from
     * @param model_name
     */
    setModelName(model) {
        this.model_name = model;
        console.log('Model is ' + model);
    }

    /**
     *Invoke aws_request_service method to request model inferences
     */
    createEndpoint() {
        // let subscrip =
        this.aws_service
            .pushFileToStorage(this.selectedFiles.item(0), this.model_name)
            .toPromise()
            .then(res => this.getResultsProxy());
        // .pipe(finalize(()=> this.getResultsProxy()))
        // .subscribe(
        // event => {
        //     console.log(event);
        // },
        // error1 => {
        //     console.log(error1);
        // },
        // () => {
        //     console.log('complete');
        //     subscrip.unsubscribe();
        // }
    }

    getResultsProxy() {
        let temp = this.getFile().name.replace('csv', 'json');

        this.bucket_inference_URL = this.bucket_inference_URL + temp;
        console.log('i am working with url : ' + this.bucket_inference_URL);
        this.getPredictionsJSON().subscribe(
            data => {
                // Create the chart
                // Create the chart
                this.data_input = JSON.parse(data.text().toString());
                //console.log(data.text());
                // Create the chart
                Highcharts.stockChart('prediction_container', {
                    rangeSelector: {
                        selected: 1
                    },

                    title: {
                        text: 'Predicted BTC Price(USD) Over Time'
                    },

                    series: [
                        {
                            name: 'Predicted BTC Price(USD)',
                            data: this.data_input,
                            type: 'spline',
                            tooltip: {
                                valueDecimals: 6
                            }
                        }
                    ]
                });
            },
            error1 => {},
            () => (this.bucket_inference_URL = 'https://s3-eu-west-1.amazonaws.com/trading-bot-bucket/predictions/')
        );
    }

    public getPredictionsJSON(): Observable<any> {
        return this.http.get(this.bucket_inference_URL);
    }
}
