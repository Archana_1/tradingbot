import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs/index';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { endpoint } from 'aws-sdk/clients/sns';

@Injectable({
    providedIn: 'root'
})
export class AwsRequestsService {
    endpoint_address = SERVER_API_URL + 'invoke_endpoint';
    constructor(private http: HttpClient) {}
    invoke_endpoint(formData): Observable<any> {
        const req = new HttpRequest('GET', this.endpoint_address, formData, {
            reportProgress: true,
            responseType: 'text'
        });
        console.log('about to invoke endpoint' + this.endpoint_address);
        return this.http.request(req);
        //return this.http.get(this.endpoint_address, inference);
    }

    pushFileToStorage(file: File, model_name: string): Observable<any> {
        let formdata: FormData = new FormData();
        formdata.append('file', file);
        formdata.append('modelName', model_name);
        const req = new HttpRequest('POST', '/invoke_endpoint', formdata, {
            reportProgress: true,
            responseType: 'text'
        });

        return this.http.request(req);
    }
}
