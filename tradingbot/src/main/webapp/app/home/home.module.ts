import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FusionChartsModule } from 'angular-fusioncharts';
import * as FusionCharts from 'fusioncharts';
import * as Charts from 'fusioncharts/fusioncharts.charts';
import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import { TradingbotSharedModule } from 'app/shared';
import { HOME_ROUTE, HomeComponent } from './';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

FusionChartsModule.fcRoot(FusionCharts, Charts, FusionTheme);

@NgModule({
    imports: [TradingbotSharedModule, RouterModule.forChild([HOME_ROUTE]), BrowserModule, FusionChartsModule, FormsModule, HttpModule],
    declarations: [HomeComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap: [HomeComponent],
    providers: []
})
export class TradingbotHomeModule {}
