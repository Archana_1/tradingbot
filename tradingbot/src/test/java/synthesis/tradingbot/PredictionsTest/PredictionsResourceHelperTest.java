package synthesis.tradingbot.PredictionsTest;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.sagemakerruntime.model.ModelErrorException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.LoggerFactory;
import synthesis.tradingbot.web.rest.PredictionsResource;
import synthesis.tradingbot.web.rest.util.PredictionsResourceHelper;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * Test class for the PredictionsResourceHelperClass
 */
public class PredictionsResourceHelperTest {
    PredictionsResourceHelper predictionsResourceHelper = new PredictionsResourceHelper();
    public static final String TAG = "PredictionsResourceHelperTests";
    String regression = "linear regression";
    String gradientBoosting = "gradient boosting";
    private final org.slf4j.Logger log = LoggerFactory.getLogger(PredictionsResource.class);
    /**
     * This tests functionality related to the createJSON method which receives two arrays and
     * transforms them into a JSON string
     */
    @Test
    public void createJSONFromTwoEqualDoubleArraysTest(){
        log.debug(TAG,"In: createJSONFromTwoEqualDoubleArraysTest()");
        PredictionsResourceHelper predictionsResourceHelper = new PredictionsResourceHelper();
        Double[] predictions = new Double[]{3.2,2.1,4.5};
        Double[] timestamp = new Double[]{10.1,22.1,400000.1};
        JSONArray expected = new JSONArray();
        JSONObject object = new JSONObject();

        object.put(timestamp[0],predictions[0]);
        expected.add(object);

        object = new JSONObject();
        object.put(timestamp[1],predictions[1]);
        expected.add(object);

        object = new JSONObject();
        object.put(timestamp[2],predictions[2]);
        expected.add(object);

        //JSONArray actual = predictionsResourceHelper.createJSON(predictions,timestamp);
        //String js_array = actual.toJSONString();
        //String temp = expected.toJSONString();
        //Assert.assertEquals(js_array,temp);
    }

    /**
     * Supplying the helper method with unequal arrays must return an empty JSON array
     * This means there's either a missing prediction or a missing timestamp
     */
    @Test
    public void createJSONFromTwoUnequalIntegersTest(){
        log.debug(TAG,"In: createJSONFromTwoUnequalIntegersTest()");
        Double[] predictions = new Double[]{3.2,2.1,4.5};
        Double[] timestamp = new Double[]{10.1,22.1};
        // Array is empty so we expect array to be empty, i.e size of actual array to be zero
        //JSONArray actual = predictionsResourceHelper.createJSON(predictions,timestamp);
        //Assert.assertEquals(0,actual.size());
    }

    /**
     * Assume we had a single prediction in this format:
     * Given a JSON string in the format : "[{
     *                                          "score": x
     *                                      }]"
     * Consider this as the base case. Justification : The implementation should be able to switch
     * between processing a JSON object(single prediction) and JSON Array(multiple objects)
     */
    @Test
    public void processOnePredictionJSONBaseCaseTest(){
        log.debug(TAG,"In: processPredictionsJSONTest()");
        // Base Case : 1 Prediction
        String json_predictions = "{\"predictions\": [{\"score\": 0.05579261854290962}]}";
        Double[] expected_predictions_array = new Double[]{0.05579261854290962};
        Double[] first_actual_results = predictionsResourceHelper.processPredictionsJSON(regression,json_predictions);
        Assert.assertEquals(expected_predictions_array,first_actual_results);

        String json_predictions_2 = "{\"predictions\": [{\"score\": 0.12}]}";
        Double[] expected_predictions_array_2 = new Double[]{0.12};
        Double[] sec_actual_results = predictionsResourceHelper.processPredictionsJSON(regression,json_predictions_2);
        Assert.assertEquals(expected_predictions_array_2,sec_actual_results);
    }
    /**
     * These tests targets the processPredictionsJSON() method
     * Given a JSON string in the format : {"predictions": [{
     *
     *                                          "score": x
         *                                      },
         *                                      {
         *                                          "score": y
         *                                      }]"
     *                                      }
     */
    @Test
    public void processManyPredictionsJSONBaseCaseTest(){
        log.debug(TAG,"In: processPredictionsJSONTest()");
        // 'General case' : More than one prediction

        String second_json_predictions = "{\"predictions\": [{\"score\": 10.1},{\"score\": 20.2},{\"score\": 18.3}]}";
        Double[] second_expected_predictions_array = new Double[]{10.1,20.2,18.3};
        Double[] second_actual_results = predictionsResourceHelper.processPredictionsJSON(regression,second_json_predictions);
        Assert.assertEquals(second_expected_predictions_array,second_actual_results);
    }

    /**
     * This test is related to the sendFileToS3() method
     * Given a File object, the method will upload the relevant file to the S3 bucket
     */
    @Test
    public void testUploadToS3Bucket(){
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
            .withRegion(Regions.EU_WEST_1)
            .withCredentials(new ProfileCredentialsProvider())
            .build();
        // TODO : Assume there are no predictions and try to upload to S3
        String bucket = "trading-bot-bucket";
        String fileName = "sample-test.txt";
        byte[] contents = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
        InputStream stream = new ByteArrayInputStream(contents);
        ObjectMetadata meta = new ObjectMetadata();
        meta.setContentLength(contents.length);
        meta.setContentType("text/csv");
        boolean isSuccessful = predictionsResourceHelper.uploadFileToS3(s3Client,stream,bucket,fileName);
        Assert.assertTrue(isSuccessful);

        // Assume the supplied bucket name is invalid or in a different region
        // We expect the result to be false
        String bucket2 = "trading-bot-temporary";
        String fileName2 = "sample-test.txt";
        byte[] contents2 = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
        InputStream stream2 = new ByteArrayInputStream(contents2);
        ObjectMetadata meta2 = new ObjectMetadata();
        meta2.setContentLength(contents.length);
        meta2.setContentType("text/csv");
        boolean isSuccessful2 = predictionsResourceHelper.uploadFileToS3(s3Client,stream2,bucket2,fileName2);
        Assert.assertFalse(isSuccessful2);
    }

    /**
     * Test whether the model endpoint can be invoked
     * 1. Supply a string formatted incorrectly, i.e : 1 0.2
     * -Expect an exception and a null prediction
     * 2. Supply a string formatted correctly, i.e : 0.055995 1:1533079080 2:0.05601 3:0.05601 4:0.05598 5:0.05598
     * -Expect a successful conversion of returned prediction
     */
    @Test
    public void testInvokeGradientBoosting(){
        predictionsResourceHelper = new PredictionsResourceHelper();
        String endpoint_name = "xgboost-28-jan-ep";
        String input_format = "text/libsvm";
        // I will supply 0.8 and I am expecting the invocation to not work because it's not in the required format
        String input = "0.03203 1:1542317880000 2:0.03203 3:0.03203 4:0.03203 5:0.03203 6:0\n0.8 1:2542290460000 2:0.3188 3:0.03188 4:0.03184 5:0.03184 6:1.5";
        Double value = null;
        try {
            String final_prediction = predictionsResourceHelper.invokeModelEndpoint(input,endpoint_name,input_format);
            value = Double.parseDouble(final_prediction);
        }catch (NumberFormatException e){
        }catch (ModelErrorException e){
        }
        Assert.assertNull(value);

        // I will supply : 0.0558 1:1533081540 2:0.0558 3:0.0558 4:0.0558 5:0.0558 and expect a prediction
        String final_prediction = predictionsResourceHelper.invokeModelEndpoint(input,endpoint_name,input_format);
        log.debug(TAG,"Just made an inference in the test class");
        Assert.assertNotNull(final_prediction);
    }

    @Test
    public void testInvokeLinearLearner(){
        predictionsResourceHelper = new PredictionsResourceHelper();
        String endpoint_name = "linear-learner-2019-01-28-06-24-21-065";
        String contentType = "text/csv";
        // I will supply 0.8 and I am expecting the invocation to not work because it's not in the required format
        String single_input = "1533071820,0.05619,0.05619,0.05611,0.05611,0.006076";
        Double value = null;
        try {
            String final_prediction = predictionsResourceHelper.invokeModelEndpoint(single_input,endpoint_name,contentType);
            //value = Double.parseDouble(final_prediction);
            Assert.assertNotNull(final_prediction);
        }catch (NumberFormatException e){
        }catch (ModelErrorException e){
        }
        String multi_line_input = "1533071820,0.05619,0.05619,0.05611,0.05611,0.006076\n1542316920000,0.03211,0.03211,0.03203,0.03203,2.085";

        try{
            String final_prediction = predictionsResourceHelper.invokeModelEndpoint(multi_line_input,endpoint_name,contentType);
            //value = Double.parseDouble(final_prediction);
            System.out.println(final_prediction);
            Assert.assertNull(value);
        }catch (NumberFormatException e){
            e.printStackTrace();
        }
//
//        // I will supply : 0.0558 1:1533081540 2:0.0558 3:0.0558 4:0.0558 5:0.0558 and expect a prediction
//        input = "0.0558 1:1533081540 2:0.0558 3:0.0558 4:0.0558 5:0.0558\n0.05377 1:1514766600 2:0.0539 3:0.0539 4:0.05364 5:0.05364 6:2.894903";
//        String final_prediction = predictionsResourceHelper.invokeModelEndpoint(input,endpoint_name,contentType);
        log.debug(TAG,"Just made an inference in the test class");
    }


}
