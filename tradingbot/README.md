# tradingbot
This application was generated using JHipster 5.6.1, you can find documentation and help at [https://www.jhipster.tech/documentation-archive/v5.6.1](https://www.jhipster.tech/documentation-archive/v5.6.1).

After cloning the project:
1. Open the project in your desired IDE.
2. In the tradingbot directory, open the terminal and run "npm install".
3. Once this is done, you can now run 'yarn run start' command to run the front end. This will also startup your browser and start the app.
